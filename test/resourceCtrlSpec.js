define(['app','angularMocks'], function(resources, mock) {
    'use strict';

        var scope, rCtrl, stateParams;
        //mock Application to allow us to inject our own dependencies

        // //mock the controller for the same reason and include $rootScope and $controller
        beforeEach(function() {
            mock.module('toerh.resources');
            mock.inject(function($rootScope, $controller, $http){

            stateParams = {page: 1};

            //create an empty scope
            scope = $rootScope.$new();
            // scope.isCurrent = function(i) {
            //     $stateParams.page = $stateParams.page || 1;
            //     return i == $stateParams.page ? 'current' : '';
            // }
            //declare the controller and inject our empty scope
            rCtrl = $controller('resourceCtrl', {$scope: scope, $http: $http, $stateParams: stateParams});

            });
        });




    describe('controller: resource', function() {


    // tests start here
        it('isCurrent function exists', function() {

            // var isCurrent = rCtrl.isCurrent(1);
            console.log(this.limit);
            this.limit = 12;
            expect(this.limit).toBe(12);
            // expect(isCurrent).toBe('current');
            expect(this.url).toBe("http://api.txs.se/v1/resources?offset=0&limit=10");

            // expect(true).toBe(true);
            // var el = $('<div></div>');

            // var app = new App(el);
            // app.render();

            // expect(el.text()).toEqual('require.js up and running');
        });

        // it('works for underscore', function() {
        //     // // just checking that _ works
        //     // expect(_.size([1,2,3])).toEqual(3);
        // });

    });

});


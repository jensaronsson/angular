var tests = [];
for (var file in window.__karma__.files) {
    if (/Spec\.js$/.test(file)) {
        tests.push(file);
    }
}

require.config({

    // Karma serves files from '/base'
    baseUrl: '/base/app/js',

    paths: {
        'angular': '../lib/angular/angular',
        'uiRouter': '../lib/angular-ui-router/release/angular-ui-router',
        'angularMocks': '../lib/angular-mocks/angular-mocks'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angularMocks': {
            deps:['angular'],
            'exports':'angular.mock'
        },
        'uiRouter': {
            deps: ['angular']
        }
    },

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});


// require(['angular', 'app', 'router'], function (angular) {
//         angular.bootstrap(document, ['toerh']);
// });
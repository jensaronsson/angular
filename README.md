ClientToerh
===========

Client applikation mot rails api.

Följande ändringar har jag gjort på api:t medans utvecklingen av denna client applikation

* Pagination
* Access tokens
* Endast autensierade användare kan posta nya resurser.
* Städar undan readme filer
* Lägga till titel och beskrivning på resurs.
* Ändrat status koder på put o delete till 200(OK) eftersom jag skickar med ett förklarande meddelande.
* Bytt till postgres pga servermiljö
* Sökfunktionalietet
* Resursers sorteras på datum i fallande ordning.
* Man kan nu få ut licenser, resurstyper och taggar.


Instruktioner:

~~Kör igång en lokal server och kör root från app/.~~
	
## UPDATE

Applikationen finns körbar på 
	http://toerhs.onjumpstarter.io/

Api:et ligger live på
	http://api.txs.se/v1/resources


Det underlättar att köra från live versionerna.



	

	
	

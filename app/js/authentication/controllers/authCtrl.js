define(['authentication/module'], function(auth) {
    'use strict';
    return auth.controller('authCtrl', ['$scope', '$http', 'authenticationService','$stateParams','sessionService',
        function($scope, $http, authService, $stateParams, sessionService) {



        $scope.credentials = {};

        $scope.submit = function() {
            authService.login(this.credentials);
        }

        $scope.logout = function() {
            authService.logout();
        }




    }]);

});


define(['authentication/module'], function(auth) {
    'use strict';
    auth.factory('authInterceptor', ['$q', 'sessionService', '$notification', function($q, sessionService, $notification) {
            return {

                request: function(req) {
                    req.headers = req.headers || {};
                    req.headers["Authorization"] = "Token token=" + "nTheDe8tv7kxLQQ4PwChexHRMQY22d8lHQhvYcz8DX9TFQgcx9g-UhekhXL13HvOJ1iPshHe";
                    if (sessionService.exists("token")) {
                        req.headers["Auth-Token"] = sessionService.get("token");
                    }
                    return req;
                },

                response: function(response) {

                    return response || $q.when(response);
                },

                responseError: function(rejection) {
                    if(rejection.status === 401) {
                        $notification.error("Unauthorized");
                    }
                    return $q.reject(rejection);
                }
            }

    }]);

    auth.config(function ($httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
    });

    return auth;

});
define(['authentication/module'], function(auth) {
    return auth.factory('userService', ['$rootScope','$http', function($rootScope, $http) {
        var baseUrl = 'http://api.txs.se/v1/users/';
        var activeUser;

        return  {
            set: function(user) {
                activeUser = user;
                $rootScope.$broadcast('user:update', user);
            },
            get: function(value) {
                console.log(activeUser);
                if(typeof(value) === "undefined") {return activeUser}
                return activeUser[value];
            },
            unset: function() {
                activeUser = {};
                $rootScope.$broadcast('user:update');
            },
            getResources: function(userId) {
                return $http.get(baseUrl + userId + '/resources');
            },
            getNext: function(url) {
                return $http.get(url);
            }

        }

    }])
})
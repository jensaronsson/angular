define(['authentication/module', 'angular'], function(auth) {
    return auth.factory('authenticationService', ['$rootScope','$http', 'sessionService', '$state','userService', '$notification',
                         function($rootScope, $http, sessionService, $state, userService, $notification) {
        var currentUser, authenticated;


        var authSession = function(data) {
            userService.set(data.user);
            authenticated = data.user.active;
        }

        var authFailed = function(data) {
            $notification.error(data.status.message);
        }



        return  {
            baseUrl: "http://api.txs.se/v1",
            login: function(credentials) {
                var login = $http.post(this.baseUrl + "/authenticate", credentials)
                login.success(function(data) {
                    sessionService.set("token", data.token),
                    userService.set(data.user);
                    authenticated = true;
                    $notification.success("Welcome!");
                    $state.go('base.resource');

                });
                login.error(function(data) {
                    console.log($notification);
                    $notification.error(data.status.message);
                });
                return login;
            },
            logout: function() {
                sessionService.unset("token");
                userService.unset();
                authenticated = "";
                $notification.info("Welcome back!");
                $state.go('base.login');
            },

            isLoggedIn: function() {
                if(sessionService.exists("token")) {
                    var isLoggedIn = $http.get(this.baseUrl + "/isloggedin");
                    isLoggedIn.success(authSession)
                    isLoggedIn.error(authFailed);
                    return isLoggedIn;
                }

            },
            isAuthenticated: function() {
                return authenticated;
            },
            getCurrentUser: function() {
                return userService.get();
            }


        }
    }]);
})
define(['resource/module'], function(auth) {
    return auth.factory('sessionService', function() {
        return {
            get: function(key) {
                return sessionStorage.getItem(key);
            },
            set: function(key, value) {
                if(key instanceof Array) {
                    for (var i = 0; i < key.length; i++) {
                        sessionStorage.setItem(key[i].name, key[i].value);
                    };
                    return;
                }
                sessionStorage.setItem(key, value);
            },
            unset: function(key) {
                if(key === "all") {
                    sessionStorage.clear();
                }
                return sessionStorage.removeItem(key);
            },
            exists: function(key) {
                return sessionStorage.hasOwnProperty(key);
            }

        }
    })
});
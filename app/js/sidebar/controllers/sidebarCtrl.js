define(['sidebar/module'], function(sidebar) {
    return sidebar.controller('sidebarCtrl', ['$scope', 'userService','authenticationService',
     function($scope, userService, authService) {
        authService.isLoggedIn();
        $scope.$on('user:update', function(event, user) {
            $scope.currentUser = user;
        })

        $scope.credentials = {};

        $scope.submit = function() {
            authService.login(this.credentials);
        }

        $scope.logout = function() {
            authService.logout();
        }

    }]);
})
define(['profile/module'], function(sidebar) {
    return sidebar.controller('profileCtrl', ['$scope', 'user','userService',
     function($scope, user, userService) {
        $scope.currentUser = user;

        userService.getResources(user.id)
        .success(function(data) {
            $scope.paginations = data.pagination;
            $scope.resources = data;
        })

        var success = function(data) {
            $scope.paginations = data.pagination
            $scope.resources = data;
        }

        $scope.pagination = function(url) {
            var result = userService.getNext(url);
            result.success(success);
        }

    }]);
})



define(['angular'], function(angular) {
    return angular.module('toerh.filters', [])
    .filter('rType', function() {
        return function(input) {
            var icons = {
                movie: "fa fa-video-camera",
                picture: "fa fa-picture-o",
                article: "fa fa-pencil",
                repo: "fa fa-bookmark-o"

            };

            return icons[input];
        }
    })
    .filter('ingress', function() {
        return function(input) {
            if(input.length > 20) {
                return input.substring(0,20) + "..";
            }
            return input;
        }
    })
})
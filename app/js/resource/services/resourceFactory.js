define(['resource/module'], function(toerh) {

    return toerh.factory('resourceFactory', ['$http', function($http) {
            var baseUrl = 'http://api.txs.se/v1/resources',
            typesUrl = 'http://api.txs.se/v1/resourcetypes',
            licenceUrl = 'http://api.txs.se/v1/licences',
            offset = '?offset=',
            limit = '&limit=',
            prev,
            next;


            return  {
                all: function(options) {
                    return $http.get(baseUrl + offset + options.offset + limit + options.limit);
                },
                get: function(id) {
                    if(id instanceof Object) {
                        var options = id;
                        return $http.get(baseUrl + offset + options.offset + limit + options.limit);
                    }
                    return $http.get(baseUrl + "/" + id);
                },
                getNext: function(url) {
                    return $http.get(url);
                },
                add: function(data) {
                    return $http.post(baseUrl, data);
                },
                getTypes: function() {
                    return $http.get(typesUrl);
                },
                getLicences: function() {
                    return $http.get(licenceUrl);
                },
                update: function(resource) {
                    return $http.put(baseUrl + "/" + resource.id, resource);
                },
                remove: function(id) {
                    return $http.delete(baseUrl + "/" + id);
                }
            }

    }]);

})
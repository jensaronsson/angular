define(['resource/module'], function(toerh) {

    return toerh.factory('searchService', ['$http', function($http) {
            var baseUrl = 'http://api.txs.se/v1/resources/search?q=',
            cached;
            return  {
                search: function(query) {
                    var result = $http.get(baseUrl + query);
                    result.success(function(data) {
                        cached = data
                    })
                    result.error(function(data) {
                        alert("något fel hände vid sökning")
                    });
                    return result;

                },
                get: function() {
                    return cached;
                }
            }

    }]);

})
define(['resource/module'], function(resource) {
    return resource.controller('searchCtrl', ['$scope','searchService', '$state','resourceFactory',
        function($scope, searchService, $state,resourceFactory) {
        $scope.query = "";
        $scope.resources = searchService.get();
        if(typeof($scope.resources) !== 'undefined') {
             $scope.paginations = $scope.resources.pagination;
        }
        var success = function(data) {

            $scope.paginations = data.pagination;
            $scope.resources = data;
        }
        $scope.pagination = function(url) {
            var result = resourceFactory.getNext(url);
            result.success(success);
        }

        $scope.search = function() {
            searchService.search(this.query)
            .then(function() {
                $state.go('base.resource.search.result');
            })
        }
    }]);
})
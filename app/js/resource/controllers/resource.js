define(['resource/module'], function(resources) {
    'use strict';

     return resources.controller('resourceCtrl',
        ['$scope', '$http', '$stateParams', 'resourceFactory','resources',
        function($scope, $http, $stateParams, resourceFactory , resources){
            $scope.resources = resources.data;
            $scope.paginations = $scope.resources.pagination;

            var success = function(data) {

                $scope.paginations = data.pagination
                $scope.resources = data;
            }
            $scope.pagination = function(url) {
                var result = resourceFactory.getNext(url);
                result.success(success);
            }



    }]);
});



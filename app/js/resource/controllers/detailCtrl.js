define(['resource/module'], function(resource) {
    return resource.controller('detailCtrl', ['$scope', 'detail', 'resourceFactory','userService', '$state', '$notification',
     function($scope, detail, resourceFactory, userService, $state, $notification) {
        $scope.currentUser = userService.get();
        $scope.resource = detail.data;


          $scope.delete = function(id) {
            if(confirm("Are you sure?")) {
                resourceFactory.remove(id).
                success(function(data) {
                    $notification.success(data.status.message);
                    $state.transitionTo('base.resource', {}, { reload: true, inherit: false, notify: true });
                })
            }

          }



    }]);
})

define(['resource/module', 'lodash'], function(resource, _) {

return resource.controller('addCtrl', ['$scope', 'resourceTypes', 'licences',
                                         'resourceFactory', 'userService','$state', 'detail', '$notification',
                function($scope, resourceTypes, licences, resourceFactory, userService, $state, detail, $notification) {

                    if(detail === null) {
                        detail = {};
                        detail.data = {
                            licence_id: "",
                            resourcetype_id: "",
                            title: "",
                            description: "",
                            url: "",
                            tags: [],
                            user_id: userService.get("id")
                        }
                    }

                    console.log(detail);
                    $scope.resourceTypes = resourceTypes.data;
                    $scope.licences = licences.data;
                    // $scope.tags = detail.data.tags;
                    $scope.tags = [];
                    _(detail.data.tags).forEach(function(val) {
                        $scope.tags.push(val.tag);
                    })

                    $scope.resource = detail.data;
                    console.log($scope.resource);
                    $scope.resource.tags = $scope.tags;







                    $scope.addTag = function(tag) {
                        if(typeof(tag) === 'undefined') return;
                        if(_.contains(this.tags, tag)) {
                            alert("Hey, the tag already exist in list!");
                            return;
                        }
                        $scope.tags.push(tag);

                    }

                    $scope.add = function() {
                        resourceFactory.add(this.resource)
                        .success(function(data) {
                             $state.transitionTo('base.resource.detail',
                                {resourceId: data.status.resource}, { reload: true, inherit: false, notify: true });
                             $notification.success(data.status.message);
                        })
                        .error(function(data) {
                            $notification.error(data.status.message);
                        })
                    }


                      $scope.update = function() {
                        resource = {
                            id: $scope.resource.id,
                            licence_id: $scope.resource.licence.id,
                            resourcetype_id: $scope.resource.resourcetype.id,
                            title: $scope.resource.title,
                            description: $scope.resource.description,
                            url: $scope.resource.url,
                            tags: $scope.tags,
                            user_id: userService.get("id")
                        }

                        resourceFactory.update(resource)
                        .success(function(data) {
                            $state.transitionTo('base.resource.detail',
                                {resourceId: data.status.resource}, { reload: true, inherit: false, notify: true });
                        })
                      }

                }]);
});

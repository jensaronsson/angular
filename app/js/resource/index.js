define(['./controllers/resource',
        './services/resourceFactory',
        './services/searchService',
        './directives/tags',
        './directives/pagination',
        './controllers/searchCtrl',
        './controllers/addCtrl',
        './controllers/detailCtrl'], function() {
});
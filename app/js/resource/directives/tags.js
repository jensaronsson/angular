define(['resource/module'], function (resource) {
        return resource.directive('tags', function() {
            return  {
                restrict: 'E',
                templateUrl: 'js/resource/templates/tagsDirective.html',
                scope: {
                  addTag: '&',
                  tagValue: '@',
                  listTags: '@',
                  tagLists: '@',
                  myTags: '='
                },
                link: function (scope, elem) {

                    scope.localAddTag = function(tag) {
                         addTag(tag);
                    };

                    elem.bind('keydown', function (e) {
                        if(e.which === 32 || e.which === 9 ) {
                            if(scope.tagValue.trim().length === 0) return;
                            addTag(scope.tagValue);
                            e.preventDefault();
                        }
                    })

                    var addTag  = function(tag) {
                        if(typeof(tag) === 'undefined') return;
                        if(tag instanceof Object) {
                            if(typeof(tag.tagm) === 'undefined') return;
                            tag = tag.tagm;
                        };
                        scope.addTag({tagm: tag});
                        elem.find('input').val("");
                        scope.$apply();
                    }
                },


            }
        })
    }
)
define([
    'app',
    'lodash'
], function(toerh, _) {

     toerh.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise("/resources");
                $stateProvider
                    .state('base',
                    {
                        auth: false,
                        abstract: true,
                        url: '',
                        views: {
                            'sidebar' : {
                                templateUrl: 'js/common/templates/sidebar.html',
                                controller: 'sidebarCtrl'
                            }
                        }
                    })
                    .state('base.resource',
                    {
                        url:  '/resources',
                        resolve: {
                            resources: function(resourceFactory) {
                                return resourceFactory.all({offset: 0, limit: 10});
                            }
                        },
                        auth: false,
                        views : {
                            'main@': {
                                templateUrl: 'js/resource/templates/main.html',
                                controller: 'resourceCtrl'
                            }
                        }
                    })
                    .state('base.resource.detail',
                    {
                        url: '/{resourceId:[0-9]+}',
                        resolve: {
                            detail: function(resourceFactory, $stateParams) {
                                return resourceFactory.get($stateParams.resourceId);
                            }
                        },
                        auth: false,
                        views: {
                            'second@': {
                                templateUrl: 'js/resource/templates/detail.html',
                                controller: 'detailCtrl'
                            }
                        }
                    })
                    .state('base.resource.detail.edit',
                    {
                        url: '/edit',
                        resolve: {
                            resourceTypes: function(resourceFactory) {
                                return resourceFactory.getTypes();
                            },
                            licences: function(resourceFactory) {
                                return resourceFactory.getLicences();
                            }
                        },
                        auth: true,
                        views: {
                            'second@': {
                                templateUrl: 'js/resource/templates/edit.html',
                                controller: 'addCtrl'
                            }
                        }
                    })
                    .state('base.resource.add',
                    {
                        url: '/add',
                        resolve: {
                            detail : function() {
                                return null;
                            },
                            resourceTypes: function(resourceFactory) {
                                return resourceFactory.getTypes();
                            },
                            licences: function(resourceFactory) {
                                return resourceFactory.getLicences();
                            }
                        },
                        auth: true,

                        views: {
                            'second@': {
                                templateUrl: 'js/resource/templates/add.html',
                                controller: 'addCtrl'
                            }
                        }
                    })

                    .state('base.resource.search',
                    {
                        auth: false,
                        views: {
                            '': {
                                templateUrl: 'js/resource/templates/search.html',
                                controller: 'searchCtrl'
                            }
                        }
                    })
                    .state('base.resource.search.result',
                    {
                        auth: false,
                        url: '/result',
                        views: {
                            'main@': {
                                templateUrl: 'js/resource/templates/main.html',
                                controller: 'searchCtrl'
                            }
                        }
                    })
                    .state('base.login',
                    {
                        auth: false,
                        url: '/login',
                        views: {
                            'main@': {
                                templateUrl: 'js/authentication/templates/login.html',
                                controller: 'authCtrl',
                            }

                        }
                    })
                    .state('base.profile',
                    {
                        auth: true,
                        url: '/{user:[a-zA-Z]+}',
                        resolve: {
                            user: function(userService) {
                                return userService.get();
                            }
                        },
                        views: {
                            'main@': {
                                templateUrl: 'js/profile/templates/profile.html',
                                controller: 'profileCtrl',
                            }

                        }
                    })


            }]);

        toerh.run(['$rootScope', '$state', 'authenticationService', function($rootScope, $state, authenticationService) {
          $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            console.log(authenticationService.isAuthenticated());
            if (toState.auth && !authenticationService.isAuthenticated()) {
                event.preventDefault();
                $state.go('base.login');
            };
          });
        }]);

    return toerh;


})
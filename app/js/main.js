/*global require*/
'use strict';

require.config({
    paths: {
        'angular': '../lib/angular/angular',
        'uiRouter': '../lib/angular-ui-router/release/angular-ui-router',
        'lodash': '../lib/lodash/dist/lodash.min',
        'angular-notifications' :'../lib/angular-notifications/notification'

    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'uiRouter': {
            deps: ['angular']
        },
        'angular-notifications': {
            deps: ['angular']
        }

    }
});


require(['angular', 'app', 'router'], function (angular) {
        angular.bootstrap(document, ['toerh']);
});
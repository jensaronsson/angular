/*global define*/
'use strict';
define([
    'angular',
    'uiRouter',
    'resource/index',
    'authentication/index',
    'sidebar/index',
    'profile/index',
    'common/filters/filters',
    'angular-notifications'
], function(angular) {
    'use strict';
    return angular.module("toerh", [
                                    "ui.router",
                                    "toerh.resources",
                                    "toerh.authentication",
                                    "toerh.sidebar",
                                    "toerh.profile",
                                    "toerh.filters",
                                    "notifications"
                                    ]);

});
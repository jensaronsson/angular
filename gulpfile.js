var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');
    var test;


gulp.task('css', function () {
    gulp.src('public/css/*.scss')
        .pipe(csso())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('public/dist/css'));

});

gulp.task('js', function () {
    gulp.src(['public/js/**/*.js', 'public/*.js'])
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/js/dist'));
});


gulp.task('default', function () {
    gulp.run('css', 'js');
//    gulp.watch('./_includes/css/*', function () {
//        gulp.run('css');
//    });
//
//    gulp.watch('./_includes/js/*', function () {
//        gulp.run('js');
//    });
});
